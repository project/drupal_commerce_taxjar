<?php
/**
 * @file
 * TaxJar calculation/requests functions.
 */

/**
 * Prepares the transaction request array to be sent to TaxJar.
 *
 * @param object $order
 *   The order object.
 * @param string $transaction_type
 *   The transaction type (e.g SalesOrder|SalesInvoice).
 * @param bool $commit
 *   Boolean indicating whether or not to commit the transaction.
 *
 * @return array|bool
 *   Returns the request body array to be sent, FALSE in case of failure.
 */
function commerce_taxjar_create_transaction($order, $transaction_type = 'SalesOrder', $commit = FALSE) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Return FALSE in case there are no line items.
  if ($order_wrapper->commerce_line_items->count() === 0) {
    return FALSE;
  }
  //$company_code = commerce_taxjar_company_code();
  $shipFrom = _commerce_taxjar_transaction_get_ship_from();
  $shipTo = _commerce_taxjar_transaction_get_ship_to($order);
  // Prepare the Request Body.
  $request_body = array(
    'type' => $transaction_type,
    'date' => format_date(REQUEST_TIME, 'custom', 'c'),
    'code' => 'DS-' . $order->order_id,
    //'amount' => $order->commerce_order_total['und'][0]['amount']/100,
    'shipping' => 0,
  );
  $request_body = array_merge($request_body, $shipFrom, $shipTo);
  if (!empty($commit)) {
    $request_body['commit'] = TRUE;
  }

  // For non anonymous orders, send the exemption code if it exists.
  if ($order->uid > 0) {
    // Check the Exemptions status.
    if (variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'exemptions_status', FALSE)) {
      if (isset($order_wrapper->owner->{COMMERCE_TAXJAR_EXEMPTION_CODE_FIELD})) {
        $exemption_code = $order_wrapper->owner->{COMMERCE_TAXJAR_EXEMPTION_CODE_FIELD}->value();
        if (!empty($exemption_code)) {
          $request_body['customerUsageType'] = $exemption_code;
        }
      }
    }

    // Check if the user has a VAT ID field.
    if (variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'add_vat_field', FALSE)) {
      if (isset($order_wrapper->owner->{COMMERCE_TAXJAR_VAT_ID_FIELD})) {
        $vat_id = $order_wrapper->owner->{COMMERCE_TAXJAR_VAT_ID_FIELD}->value();
        if (!empty($vat_id)) {
          $request_body['businessIdentificationNo'] = $vat_id;
        }
      }
    }
  }

  $tax_included = FALSE;
  // If the ship from country is not in the US|CA, assume the price entered
  // is "VAT-inclusive" if specified in the settings.
  if (isset($request_body['addresses']['shipFrom']['country'])) {
    if (!in_array($request_body['addresses']['shipFrom']['country'], array('CA', 'US'))) {
      $tax_included = variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'vat_inclusive', TRUE);
    }
  }

  _commerce_taxjar_transaction_add_lines($request_body, $order_wrapper->commerce_line_items, $tax_included);
  drupal_alter('commerce_taxjar_create_transaction', $request_body, $order);
  return $request_body;
}

/**
 * Returns the transaction "lines" that needs to be sent to the API.
 *
 * @param array $request_body
 *   The request body that needs to be altered.
 * @param array $line_items
 *   An array of line items wrapper that need to be added to the transaction.
 * @param bool $tax_included
 *   Boolean indicating whether or not the tax is included.
 */
function _commerce_taxjar_transaction_add_lines(&$request_body, $line_items, $tax_included = FALSE) {
  $lines = array();
  $tax_included = $tax_included ? 'true' : 'false';
  // Loop over the line items passed.
  foreach ($line_items as $delta => $line_item_wrapper) {

    // Ensure the line item still exists.
    if (!$line_item_wrapper->value()) {
      continue;
    }
    $line_item = $line_item_wrapper->value();

    $discounted = 'false';
    // Handles products.
    if (in_array($line_item->type, commerce_product_line_item_types())) {
      $tax_code = 99999;
      $line_item_unit_price = $line_item_wrapper->commerce_product->commerce_price->amount->value();
      $discount = 0;
      if ($line_item_wrapper->type->value() == 'product') {
        // for discount line items
        // get discount display title (I suspect there might be a better way than this)
        $discount_data = $line_item_wrapper->commerce_unit_price->data->value();
        foreach ($discount_data['components'] as $key => $component) {
          if (!empty($component['price']['data']['discount_component_title'])) {
            $discount = $component['price']['amount']* -1;
          }
        }
      }
      // Get the tax code from the "Tax code" term referenced by the product.
      if (isset($line_item_wrapper->commerce_product->commerce_taxjar_code)) {
        if ($line_item_wrapper->commerce_product->commerce_taxjar_code->value()) {
          $tax_code = $line_item_wrapper->commerce_product->commerce_taxjar_code->name->value();
        }
      }
      // line item unit price
      $line_item_unit_price = $line_item_wrapper->commerce_product->commerce_price->amount->value();
      $lines[] = array(
        'id' => $line_item->line_item_id,
       // 'id' => $delta + 1,
       // 'itemCode' => $line_item_wrapper->commerce_product->sku->value(),
      //  'description' => $line_item_wrapper->commerce_product->title->value(),
        'product_tax_code' => $tax_code,
        'quantity' => $line_item->quantity,
        'unit_price' => $line_item_unit_price/100,
        // The discounted boolean needs to be set to TRUE, otherwise, discount
        // document level won't be applied.
        'discount' => $discount/100 * $line_item->quantity,
      //  'taxIncluded' => $tax_included,
      );
    }
    elseif ($line_item->type === 'shipping') {
      $line_item_unit_price = $line_item_wrapper->commerce_unit_price->amount->value();
      $request_body['shipping'] = $line_item_unit_price/100;
     /* $lines[] = array(
        'id' => $line_item->line_item_id,
        'number' => $delta + 1,
        'itemCode' => 'Shipping',
        'description' => 'Shipping',
        // Retrieve the configured Shipping tax code.
        'product_tax_code' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'shipcode', '999999'),
        'quantity' => $line_item->quantity,
        'unit_price' => $line_item_unit_price/100,
        // Shipping shouldn't be discounted.
        'discounted' => 'false',
      );*/
    }
  }
  if ($lines) {
    $request_body['line_items'] = $lines;
  }
}

/**
 * Helper function used to determine the customerCode sent.
 */
function _commerce_taxjar_transaction_get_customer_code($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get User name or e-mail address.
  if ($order->uid === 0) {
    if ($order->mail == '') {
      $customer_code = 'administrator';
    }
    else {
      $user_email = $order->mail;
      $customer_code = commerce_taxjar_email_to_username($user_email);
    }
  }
  else {
    $customer_code = $order_wrapper->owner->name->value();
  }

  return $customer_code;
}

/**
 * Returns the shipFrom address for a transaction.
 */
function _commerce_taxjar_transaction_get_ship_from() {
  return array(
    'from_street' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_street1', '').' '.variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_street2', '') ,
    'from_city' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_city', ''),
    'from_state' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_state', ''),
    'from_country' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_country', ''),
    'from_zip' => variable_get(COMMERCE_TAXJAR_VAR_PREFIX . 'primary_zip', ''),
  );
}

/**
 * Returns the shipTo address for a transaction.
 */
function _commerce_taxjar_transaction_get_ship_to($order) {
  $ship_to = array();
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $customer_profile_field = commerce_taxjar_get_customer_profile_field();

  // Retrieve the address from the configured customer profile type.
  if (!empty($customer_profile_field) && !empty($order->{$customer_profile_field})) {
    if (isset($order_wrapper->{$customer_profile_field}->commerce_customer_address)) {
      $address = $order_wrapper->{$customer_profile_field}->commerce_customer_address->value();

      // Prepare the Ships from address.
      $ship_to = array(
        'to_street' => $address['thoroughfare'],
        'to_city' => $address['locality'],
        'to_state' => $address['administrative_area'],
        'to_country' => $address['country'],
        'to_zip' => $address['postal_code'],
      );
    }
  }

  return $ship_to;
}
